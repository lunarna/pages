#!/usr/bin/fish
for i in **.css; lightningcss -m $i -o $i; end
for i in **.xht; sed '/<!--.*-->/d' $i > $i.bak; mv $i.bak $i; end
for i in **.xht **.css **.svg; zstd -f -19 $i -o $i.zst; end
