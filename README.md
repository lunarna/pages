# anarchy.center V2
this is my personal website! please copy it and make it yours! tho it is very jank!
these are the actual resources pre-minification i may do that on the server to save on bandwidth and make the site load faster

the drawing is by Zr6Ov, license terms described on picrew: https://picrew.me/en/image_maker/1589275

i use a subset of: the [nasin-nanpa](https://github.com/ETBCOR/nasin-nanpa) font, (MIT), the [blooming grove](https://www.dafont.com/bloominggrove.font) font (public domain), and a subset of [fantasque sans mono bold](https://github.com/belluzj/fantasque-sans) (ofl 1.1).
i use a derivation of the [atkinson hyperlegible](https://brailleinstitute.org/freefont) font (atkinson hyperlegible font license) with the old-style numerals from the [inter alia](https://github.com/Shavian-info/interalia) font (ofl 1.1).
i minified and compressed these fonts down to tiny sizes and they should not affect browsing speed

volpeon's neocat (cc-by-nc-sa 4.0) https://volpeon.ink/emojis/neocat/ and tyson tan's kate the cyber woodpecker (cc-by-sa 4.0) https://kate-editor.org/mascot/ appear in images
